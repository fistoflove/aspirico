<?php
/**
 * Child Starter functions and definitions
 *
 */


// WP Admin bar style fix


  add_action('wp_head', function() {
	?>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Figtree:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://i.icomoon.io/public/32fdff22a3/Aspirico/style.css">
	<link rel="icon" type="image/x-icon" href="/wp-content/themes/aspirico/favicon.ico">
	<?php
  });

  add_action('wp_head', function() {
	$user = wp_get_current_user();
	if($user->exists()) {
		if(str_contains($user->user_email, '@imsmarketing.ie')) {
			?>
			<script>
			  window.markerConfig = {
				project: '63ca43dc404916063b8eb48d', 
				source: 'snippet',
				reporter: {
					email: 'vini@imsmarketing.ie',
					fullName: 'IMS Marketing',
				},
			  };
			</script>
			<?php
		} else {
			?>
			<script>
			  window.markerConfig = {
				project: '63ca43dc404916063b8eb48d', 
				source: 'snippet',
				reporter: {
					email: '<?= $user->user_email; ?>',
					fullName: '<?= $user->display_name; ?>',
				},
			  };
			</script>
			<?php
		}
		?>
		<script>
			!function(e,r,a){if(!e.__Marker){e.__Marker={};var t=[],n={__cs:t};["show","hide","isVisible","capture","cancelCapture","unload","reload","isExtensionInstalled","setReporter","setCustomData","on","off"].forEach(function(e){n[e]=function(){var r=Array.prototype.slice.call(arguments);r.unshift(e),t.push(r)}}),e.Marker=n;var s=r.createElement("script");s.async=1,s.src="https://edge.marker.io/latest/shim.js";var i=r.getElementsByTagName("script")[0];i.parentNode.insertBefore(s,i)}}(window,document);
		</script>
		<?php
	}
  });



// WP Admin bar style fix

add_action('wp_head', function() {
	if(is_user_logged_in()) {
	  echo "<style>html { margin-top:0px !important;}</style>";
	}
  }, 100);


add_action( 'login_enqueue_scripts', function() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/logo.svg);
			height: 185px;
			width: 285px;
			background-size: 285px 185px;
			background-repeat: no-repeat;
        }
    </style>
<?php });