<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Left Tab",
    [
        ["Title", "text"],
        ["Items", "repeater", [
            ["Title", "text"],
            ["Content", "wysiwyg"]
        ]]
    ]
);

$fields->register_tab(
    "Right Tab",
    [
        ["Title", "text"],
        ["Items", "repeater", [
            ["Title", "text"],
            ["Content", "wysiwyg"]
        ]]
    ]
);