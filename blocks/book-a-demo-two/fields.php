<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Top Text", "text"],
        ["Bottom Text", "text"],
        ["Form ID", "text"]
    ]
);