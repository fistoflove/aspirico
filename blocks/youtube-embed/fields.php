<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Video",
    [
        ["Youtube Video ID", "text"],
    ]
);

$fields->register_tab(
    "Top",
    [
        ["Content", "wysiwyg"],
    ]
);

$fields->register_tab(
    "Bottom",
    [
        ["Content", "wysiwyg"],
    ]
);