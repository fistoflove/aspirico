<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Link", "link"],
        ["Boxes", "repeater", [
            ["Content", "wysiwyg"],
        ]],
    ]
);