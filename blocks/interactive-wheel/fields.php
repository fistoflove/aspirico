<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Top Left",
    [
        ["Title", "wysiwyg"],
        ["Items", "repeater", [
            ["Label", "text"],
            ["Icon", "image"],
        ]],
    ]
);

$fields->register_tab(
    "Top Right",
    [
        ["Title", "wysiwyg"],
        ["Items", "repeater", [
            ["Label", "text"],
            ["Icon", "image"],
        ]],
    ]
);

$fields->register_tab(
    "Bottom Left",
    [
        ["Title", "wysiwyg"],
        ["Items", "repeater", [
            ["Label", "text"],
            ["Icon", "image"],
        ]],
    ]
);

$fields->register_tab(
    "Bottom Right",
    [
        ["Title", "wysiwyg"],
        ["Items", "repeater", [
            ["Label", "text"],
            ["Icon", "image"],
        ]],
    ]
);