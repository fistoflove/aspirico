<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Items", "repeater", [
            ["Name", "text"],
            ["Position", "text"],
            ["Logo", "image"],
            ["Content", "wysiwyg"],
            ["Link", "link"],
        ]],
    ]
);