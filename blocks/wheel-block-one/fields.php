<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Box Label One", "text"],
        ["Box Label Two", "text"],
        ["Box Label Three", "text"],
        ["Box Label Four", "text"],
        ["Box Label Five", "text"],
    ]
);